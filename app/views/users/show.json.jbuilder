json.extract! @user, :id, :name, :birthdate, :gender, :height, :weight, :created_at, :updated_at

json.activities do 
    json.array!(@user.activities) do |activity|
      json.extract! activity, :id, :name
      json.url activity_url(activity, format: :json)
    end
end

