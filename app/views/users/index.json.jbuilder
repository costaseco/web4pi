json.array!(@users) do |user|
  json.extract! user, :id, :name, :birthdate, :gender, :height, :weight
  json.url user_url(user, format: :json)
end
