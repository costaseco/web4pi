# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


john = User.create({name:"John", birthdate:"1980-11-05", gender:"M", height:180, weight:70})
mary = User.create({name:"Mary", birthdate:"1983-05-20", gender:"F", height:175, weight:67})

walk = Kind.create({name:"Walk"})
hike = Kind.create({name: "Hike"})
run = Kind.create({name:"Run"})

Activity.create([
    {name: "By the sea", date:"2016-04-25", user:john, kind:walk, distance:6, time:60},
    {name: "Up Hill", date:"2016-04-10", user:mary, kind:hike, distance:10, time:100}
])