class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.datetime :date
      t.integer :user_id
      t.integer :kind_id
      t.integer :distance
      t.integer :time

      t.timestamps null: false
    end
  end
end
