
# Web Development with Ruby'n Rails

This is a quick introduction to web development using Ruby'n Rails.

# Further reading and reference

General documentation

- [Try Ruby website](http://www.tryruby.org)
- Lecture Slides
- [Rails Guides](http://guides.rubyonrails.org)

- [Adding bootstrap to rails](https://launchschool.com/blog/integrating-rails-and-bootstrap-part-1/)

- [Adding devise](https://github.com/plataformatec/devise)

- [General Setup](https://gorails.com/)

# Example application

**Outdoor Activities and pedestrian tracks**

<!-- ## Data model

```
User name:string 
Activity name:string date:datetime user_id:integer distance:integer time:integer

```

 -->

# Script steps

The first step is to create a new (empty) application

```
rails new tracks
rails server
```

After a few minutes setting up, it is up and running at `localhost:3000`. 

Let's extend it with a homepage

```
cd tracks
rails generate controller welcome index
```

Change `routes.rb` and uncomment the following line:

```
root welcome#index
```

Don't forget, use git and commit every step of the way

# Enhance the presentation by adding Bootstrap

Add the import declaration to the gem file

```
gem 'bootstrap-sass'
```

Execute the bundler to incorporate the actual code

```
bundle install
```

Rename file `application.css` to `application.css.sass`

and include the following lines:

```
@import "bootstrap-sprockets"
@import "bootstrap"
```

Also, change the contents of file `application.js` to

```
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .
```

# Change the application layout

Go to the file `application.html.erb` and change the core of the page to:

```
    <div id="main-container" class="container">
      <div class="row">
        <div class="col-xs-3">
          <h3>Tracks application</h3>
        </div>
        <div class="col-xs-9">
          <%= yield %>
        </div>
      </div>
    </div>
```

# Data Model 

Let's install the data model and corresponding management interface

```
rails generate scaffold User name:string birthdate:date gender:string height:integer weight:integer
rails generate scaffold Kind name:string
rails generate scaffold Activity name:string date:datetime user_id:integer kind_id:integer distance:integer time:integer
rake db:migrate
```

Now, take some time to inspect the code that was automatically generated.

# Seed data

We may interact with the ActiveRecord console to play with the data, just run on the system terminal

```
rails console
``` 

You can then insert ActiveRecord expressions like:

```
User.create({name:"John", birthdate:"1980-11-05", gender:"M", height:180, weight:70})
User.create({name:"Mary", birthdate:"1983-05-20", gender:"F", height:175, weight:67})
User.all
```

or you can add all interesting data to the file `seeds.rb` and use it 

```
User.create(
[{name:"John", birthdate:"1980-11-05", gender:"M", height:180, weight:70},
 {name:"Mary", birthdate:"1983-05-20", gender:"F", height:175, weight:67}])

Kind.create([{name:"Walk"},{name: "Hike"},{name:"Run"}])

Activity.create([
    {name: "By the sea", date:"2016-04-25", user_id:1, kind_id:1, distance:6, time:60},
    {name: "Up Hill", date:"2016-04-10", user_id:2, kind_id:2, distance:10, time:100}
])
```

and run 

```
rake db:seed
```

Now, experiment with the application REST interface and inspect the controller code that produced them.

# Model associations

ActiveRecord allows one to extend the model with association information. We know that there is a 1-N relationship between Activities and Users and Activities and Kinds. 

Check the class in file `activity.rb` and change it to represent N-1 side of the relationship.

```
class Activity < ActiveRecord::Base
    belongs_to :user
    belongs_to :kind
end
```

Experiment in rails console to evaluate 

```
Activity.find(1).kind
```

The same thing happens with the other entities but representing the 1-N side of the relationship. Change the file `kind.rb` to

```
class Kind < ActiveRecord::Base
    has_many :activities
end
```

and file `user.rb` to

```
class User < ActiveRecord::Base
    has_many :activities
end
```

Notice the pluralization of the names.

Now, we can change the seed data file `seeds.rb` to a more elegant OO style of object creation.

```
john = User.create({name:"John", birthdate:"1980-11-05", gender:"M", height:180, weight:70})
mary = User.create({name:"Mary", birthdate:"1983-05-20", gender:"F", height:175, weight:67})

walk = Kind.create({name:"Walk"})
hike = Kind.create({name: "Hike"})
run = Kind.create({name:"Run"})

Activity.create([
    {name: "By the sea", date:"2016-04-25", user:john, kind:walk, distance:6, time:60},
    {name: "Up Hill", date:"2016-04-10", user:mary, kind:hike, distance:10, time:100}
])
```


This really helps when developing our controllers. Refer to the documentation about model associations on [rails site](http://guides.rubyonrails.org/association_basics.html).


# Change the application layout

Go to the file `application.html.erb` and change the core of the page to:

```
    <div id="main-container" class="container">
      <div class="row">
        <div class="col-xs-3">
          <h3>Tracks application</h3>
        </div>
        <div class="col-xs-9">
          <%= yield %>
        </div>
      </div>
    </div>
```

# Use associations to improve interface

Forms are built using helpers, see file `_form.html.erb` in the `views/activities` folder.

Look for the field that implements the relation between activities and users. Instead of the code 

```
  <div class="field">
    <%= f.label :user_id %><br>
    <%= f.number_field :user_id %>
  </div>
```

use a dynamically built select box that uses the model to retrieve all possible users to assign to an activity

```
  <div class="field">
    <%= f.select(:user_id, User.all.collect {|u| [u.name, u.id]}) %>
  </div>
```

Later we may want to modify this to always be the logged in user.

Repeat this for the kind of activity.

```
  <div class="field">
    <%= f.select(:kind_id, Kind.all.collect {|a| [a.name, a.id]}) %><br>
  </div>
```

Also, you can change the show view to present the user name and a link to it.

Check file `show.html.erb` (under `views/activities`) and change the parts corresponding to user and kind to the following code:

```
<p>
  <strong>User:</strong>
  <%= link_to @activity.user.name, @activity.user %>
</p>

<p>
  <strong>Kind:</strong>
  <%= link_to @activity.kind.name, @activity.kind %>
</p>
```

To finish the small presentation, you may want to present the list of activities of a certain user or a kind of activities. Check file `show.html.erb` for a user (under `views/users`).

Add a section to the file with the following code:

```
<h2>Activities</h2>
<p>
<ul>
<% @user.activities.each do |activity| %>
<li> <%= link_to activity.name, activity %> </li>
<% end %>
</ul>
</p>
```

Repeat for the entity kind.

<!-- # Attach a track to an activity

(followed closely [this tutorial](http://larsgebhardt.de/parse-xml-with-ruby-on-rails-paperclip-and-nokogiri/))

We need to upload a gpx file and store the track points on the database first


```
rails generate model Point name:string latitude:float longitude:float elevation:float description:string point_created_at:datetime activity_id:integer
```





upload a file

see a map
 -->


# Security - Authentication in a Web Application

Now, let's install one more gem to implement security

Add to the gem file 

```
gem 'devise'
```

Execute 

```
bundle install
rails generate devise:install
```

read the instructions given and act on them if needed.

You can use the following command to associate (or create) a given model (here `User`) as the center of the authentication model

```
rails generate devise User
rake db:migrate
```

This gem installs a lot of new functions to handle user authentication 


